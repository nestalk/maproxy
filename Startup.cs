﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MaProxy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private const int ReadChunkBufferLength = 4096;

        public void ConfigureServices(IServiceCollection services)
        {
            var proxyBuilder = services.AddReverseProxy();
            proxyBuilder.LoadFromConfig(Configuration.GetSection("ReverseProxy"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapReverseProxy(proxyPipeline =>
                {
                    proxyPipeline.Use(async (context, next) =>
                    {
                        var originalBody = context.Response.Body;

                        using var requestStream = new MemoryStream();

                        using var responseStream = new MemoryStream();
                        context.Response.Body = responseStream;
                        context.Request.EnableBuffering();

                        await context.Request.Body.CopyToAsync(requestStream);

                        Console.WriteLine($"Http Request Information:{ Environment.NewLine}" +
                        $"Schema:{context.Request.Scheme} { Environment.NewLine}" +
                            $"Host: {context.Request.Host} { Environment.NewLine}" +
                            $"Path: {context.Request.Path} { Environment.NewLine}" +
                            $"QueryString: {context.Request.QueryString} { Environment.NewLine}" +
                            $"Response Body: {ReadStreamInChunks(requestStream)}{ Environment.NewLine}");

                        context.Request.Body.Position = 0;

                        await next();

                        context.Response.Body.Position = 0;
                        await responseStream.CopyToAsync(originalBody);

                        Console.WriteLine($"Http Response Information:{ Environment.NewLine}" +
                        $"Schema:{context.Request.Scheme} { Environment.NewLine}" +
                            $"Host: {context.Request.Host} { Environment.NewLine}" +
                            $"Path: {context.Request.Path} { Environment.NewLine}" +
                            $"QueryString: {context.Request.QueryString} { Environment.NewLine}" +
                            $"Response Body: {ReadStreamInChunks(responseStream)}{ Environment.NewLine}");


                    });
                });
            });
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            string result;
            using (var textWriter = new StringWriter())
            using (var reader = new StreamReader(stream))
            {
                var readChunk = new char[ReadChunkBufferLength];
                int readChunkLength;
                //do while: is useful for the last iteration in case readChunkLength < chunkLength
                do
                {
                    readChunkLength = reader.ReadBlock(readChunk, 0, ReadChunkBufferLength);
                    textWriter.Write(readChunk, 0, readChunkLength);
                } while (readChunkLength > 0);

                result = textWriter.ToString();
            }

            return result;
        }

    }
}
